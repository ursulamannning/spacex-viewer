import { gql } from '@apollo/client';

export const QUERY_LAUNCH_LIST = gql`
	query LaunchList {
		launches {
			mission_name
      rocket {
        rocket_name
      }
      launch_date_utc
		}
	}
`;