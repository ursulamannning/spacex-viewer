import React from 'react';
import { useQuery } from '@apollo/client';
import { QUERY_LAUNCH_LIST } from './query';
import FlightInfo from '../FlightInfo/FlightInfo';

const LaunchList = function() {
  const { loading, error, data } = useQuery(QUERY_LAUNCH_LIST);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Unable to load page: </p>;

  return data.launches.map(({ mission_name, rocket, launch_date_utc }, idx) => (
    <FlightInfo
      listId={idx+1}
      missionName={mission_name}
      rocketName={rocket.rocket_name}
      launchDateUTC={launch_date_utc}
    />
  ));
}

export default LaunchList;