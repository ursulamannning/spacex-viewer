import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { parseISO, format } from 'date-fns';
// import './_launchItem.css';

class FlightInfo extends Component {
  render() {
    const { listId, missionName, rocketName, launchDateUTC } = this.props,
          date = format(parseISO(launchDateUTC), 'do LLL yyyy');

    return (
      <li>
        <div className={'content'}>
          <div className={'listId'}>#{listId}</div>
          <div className={'missionName'}>{missionName}</div>
          <div className={'info'}>
            <div>{date}</div>
            <div className={'rocketName'}>{rocketName}</div>
          </div>
        </div>
      </li>
    );
  }
}

FlightInfo.propTypes = {
  listId: PropTypes.number,
  missionName: PropTypes.string,
  rocketName: PropTypes.string,
  launchDateUTC: PropTypes.string
};

export default FlightInfo;