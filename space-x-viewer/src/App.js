import React from 'react';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';
import LaunchList from './components/LaunchList/LaunchList';
import './App.css';

const client = new ApolloClient({
  uri: 'http://api.spacex.land/graphQL/',
  cache: new InMemoryCache()
});

const App = () => {
  <ApolloProvider client={client}>
    <div className="App">
      <p>Launches</p>
      <LaunchList/>
    </div>
  </ApolloProvider>
}

export default App;